FROM mgood/appengine-python:1.9.28

ADD nodesource.* /tmp/

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-key add /tmp/nodesource.gpg.key && \
    rm /tmp/nodesource.gpg.key && \
    apt-get update && \
    apt-get install -y --force-yes \
      apt-transport-https && \
    mv /tmp/nodesource.list /etc/apt/sources.list.d/ && \
    apt-get update && \
    apt-get install -y --force-yes \
      lsof \
      curl \
      make \
      git \
      nodejs && \
    npm install -g ember-cli bower
